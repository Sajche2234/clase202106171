package com.edutec.clase202106171;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityDiv extends AppCompatActivity {
    private EditText campo1, campo2;
    private TextView txtResultadoo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_div);
        campo1 = findViewById(R.id.txtN1);
        campo2 = findViewById(R.id.txtN2);
        txtResultadoo = findViewById(R.id.txtResultado);
    }
    private void division(){
        String num1 = campo1.getText().toString();
        String num2 = campo2.getText().toString();
        double resultado=0;
        if(!num1.isEmpty() && !num2.isEmpty()){
            resultado = Double.parseDouble(num1) / Double.parseDouble(num2);
            txtResultadoo.setText("RESULTADO: "+resultado);
        } else {
            Toast.makeText(this, "Campos Vacios", Toast.LENGTH_LONG).show();
        }
    }
    public void onDividir(View view) {
        if(view.getId() == R.id.btnDiv){
            division();
        }
    }
}