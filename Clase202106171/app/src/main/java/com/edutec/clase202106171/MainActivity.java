package com.edutec.clase202106171;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClic(View view) {
        if(view.getId() == R.id.btnUno){
            Intent intent = new Intent(this, MainActivity2.class);
            startActivity(intent);
        }
        if(view.getId() == R.id.btnResta){
            Intent inResta = new Intent(this, ActivityResta.class);
            startActivity(inResta);
        }
        if(view.getId() == R.id.btnMult){
            Intent inMult = new Intent(this, ActivityMult.class);
            startActivity(inMult);
        }
        if(view.getId() == R.id.btnDiv){
            Intent inDiv = new Intent(this, ActivityDiv.class);
            startActivity(inDiv);
        }
    }
}