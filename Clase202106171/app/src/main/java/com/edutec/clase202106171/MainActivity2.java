package com.edutec.clase202106171;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity2 extends AppCompatActivity {
    // Componentes definidos en la declaracion de variables
    private EditText campo1, campo2;
    private TextView txtResultadoo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        // Activar los componentes para poder ser utilizados en el Main
        campo1 = findViewById(R.id.txtN1);
        campo2 = findViewById(R.id.txtN2);
        txtResultadoo = findViewById(R.id.txtResultado);
    }
    // Método a utilizar
    private void sumar(){
        String num1 = campo1.getText().toString();
        String num2 = campo2.getText().toString();
        int resultado=0;
        if(!num1.isEmpty() && !num2.isEmpty()){
            resultado = Integer.parseInt(num1) + Integer.parseInt(num2);
            txtResultadoo.setText("RESULTADO: "+resultado);
        } else {
            Toast.makeText(this, "Campos Vacios", Toast.LENGTH_LONG).show();
        }
    }

    public void onClic(View view) {
        if(view.getId() == R.id.btnSumar){
            sumar();
        }
    }
}